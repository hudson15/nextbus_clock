
#include <ArduinoJson.h>

class JsonFilters{

    public: 
        static DeserializationOption::Filter httpGetJsonForStop();
        static DeserializationOption::Filter getAgencyList();
        static DeserializationOption::Filter getRouteList();
        static DeserializationOption::Filter routeGeographicBounds();
};
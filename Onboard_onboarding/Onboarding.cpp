#include <Onboarding.h>
#include <MyHTTPClient.h>
#include <ArduinoJson.h>
#include <JsonFilters.h>
void Onboarding::printGeographicBounds()
{   
    Serial.print(F("start:"));
    Serial.println(ESP.getFreeHeap()); //40792, 41144
    MyHTTPClient client;

    // client.getAgencyList()

    const String AGENCY_TAG = "chapel-hill";
    const String JSONroutelist = client.getRouteList(AGENCY_TAG);
    //arduinojsonv5 parser

    Serial.print(F("prebuffer:"));
    Serial.println(ESP.getFreeHeap()); // prebuffer: 39816, 40040
    
    const size_t capacity = JSON_ARRAY_SIZE(24) + 25 * JSON_OBJECT_SIZE(2) + 520;
    DynamicJsonDocument doc(capacity);
    deserializeJson(doc, JSONroutelist);

    Serial.print(F("post-buffer:"));
    Serial.println(ESP.getFreeHeap()); // pre-vector:34336, 38328
    
    std::vector<Latlng> locations;
    for (JsonVariant route_item : doc["route"].as<JsonArray>())
    {       
            const String JSONrouteconfig = client.getRouteConfig(AGENCY_TAG,route_item["tag"]);
            BoundingBox box = getLatLongFromJson(JSONrouteconfig);
            locations.push_back(box.southWestCorner);
            locations.push_back(box.northEastCorner);
    }

    Serial.print(F("post-routeConfig:"));
    Serial.println(ESP.getFreeHeap()); //

    BoundingBox bigBounds = determineBounds(locations);
    Serial.printf("SouthWest lat: %f, lng: %f\n", bigBounds.southWestCorner.latatude, bigBounds.southWestCorner.longitude);
    Serial.printf("NorthEast lat: %f, lng: %f", bigBounds.northEastCorner.latatude, bigBounds.northEastCorner.longitude);
   
};


BoundingBox Onboarding::getLatLongFromJson(String json){

    if(json.isEmpty()){
        Serial.println(F("ERROR: OOM"));
    }else{
        Serial.println(F("we made it"));
        Serial.println(json);
    }
    

    DynamicJsonDocument doc(3000);
    DeserializationError error = deserializeJson(doc, json);
    serializeJson(doc,Serial);
    if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
    abort();
    }
    JsonObject route = doc["route"];

    Serial.println(F("we made it 1.25"));

    const char* route_lonMax = route["lonMax"];               // "-79.0232569"
    const char* route_lonMin = route["lonMin"];               // "-79.0640089"
    const char* route_latMax = route["latMax"];                 // "35.9294827"
    const char* route_latMin = route["latMin"];                 // "35.9046463"
    // const char *route_color = route["color"];                 // "330000"
    // const char *route_oppositeColor = route["oppositeColor"]; // "ffffff"
    // const char* route_tag = route["tag"]; // "A"
    
    Latlng sw;
    Latlng ne;
        Serial.println(F("we made it 1.5"));
        Serial.println(route_lonMax);
        Serial.println(route_latMin);
        Serial.println(route_lonMin);
        Serial.println(route_latMax);
        
    sw.latatude = std::stod(route_latMin);
    sw.longitude = std::stod(route_lonMin);

    ne.latatude = std::stod(route_latMax);
    ne.longitude = std::stod(route_lonMax);
    Serial.println("we made it 1.75");
    return BoundingBox {ne,sw};
    
};


BoundingBox Onboarding::determineBounds(std::vector<Latlng> locations){
    Serial.println("we made it2");
    auto longitude_Extremes = std::minmax_element(locations.begin(), locations.end(),
                                     [](const Latlng& lhs, const Latlng& rhs) {
                                        return lhs.longitude < rhs.longitude;
                                     });

    auto latitude_Extremes = std::minmax_element(locations.begin(), locations.end(),
                                     [](const Latlng& lhs, const Latlng& rhs) {
                                        return lhs.latatude < rhs.latatude;
                                     });
    Serial.println("we made it3");
    Latlng upperRight{ .latatude = latitude_Extremes.first->latatude, .longitude = longitude_Extremes.first->longitude};
    Latlng lowerLeft{ .latatude = latitude_Extremes.second->latatude, .longitude = longitude_Extremes.second->longitude};

    
    BoundingBox bounds{ .northEastCorner = upperRight, .southWestCorner = lowerLeft};

    return bounds;
}
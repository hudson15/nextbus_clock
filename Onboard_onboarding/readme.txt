To obtain a list of available agencies the following command should be used:
https://retro.umoiq.com/service/publicXMLFeed?command=agencyList

(auto suggest based on location)

SAVE agency_tag TO VARIABLE

Command "routeList"
To obtain a list of routes for an agency, use the "routeList" command. The agency is specified by
the "a" parameter in the query string. The tag for the agency as obtained from the agencyList
command should be used.
The format of the command is:
https://retro.umoiq.com/service/publicXMLFeed?command=routeList&
a=<agency_tag>

PUSH all route_tags TO ARRAY

To obtain a list of routes for an agency, use the "routeConfig" command. USe &terse” if you dont want to draw the route paths

https://retro.umoiq.com/service/publicXMLFeed?command=routeConfi
g&a=<agency_tag>&r=<route tag>&terse” 

FOR EACH ROUTE, PUSH STOPS INTO AN ARRAY.
While doing this, keep track of the most extreme points of
latMin, latMax, lonMin, lonMax – specifies the extent of the route
request a map from open street map.

https://gis.stackexchange.com/questions/49805/can-i-view-a-google-maps-map-by-bounding-latitude-and-longitude-coordinates 


slap all the stops on it with stop[lat/lon] – specify the location of the stop.
Use location services to place the gps and suggest the closest stop.







#include <JsonFilters.h>


DeserializationOption::Filter httpGetJsonForStop(){
    StaticJsonDocument<64> filter;
    filter["list"][0]["temperature"] = true; //not implemented
    return DeserializationOption::Filter(filter);
}


DeserializationOption::Filter getAgencyList(){
    StaticJsonDocument<64> filter;
    filter["list"][0]["temperature"] = true; //not implemented
    return DeserializationOption::Filter(filter);
}

DeserializationOption::Filter getRouteList(){
    StaticJsonDocument<64> filter;
    filter["route"][0]["tag"] = true;
    return DeserializationOption::Filter(filter);
}

DeserializationOption::Filter routeGeographicBounds(){
    StaticJsonDocument<64> filter;
    filter["route"]["latMax"] = true;
    filter["route"]["latMin"] = true;
    filter["route"]["lonMax"] = true;
    filter["route"]["lonMin"] = true;
    return DeserializationOption::Filter(filter);
}

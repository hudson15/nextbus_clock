#include <Arduino.h>
#include <String>
#include <GeoStruts.h>

class Onboarding
{
    public:
    void printGeographicBounds();
private:
    BoundingBox getLatLongFromJson(String json);
    BoundingBox determineBounds(std::vector<Latlng> locations);
};
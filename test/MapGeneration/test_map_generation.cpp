#include <Onboarding.h>
#include <unity.h>

// void setUp(void) {
// // set stuff up here
// }

// void tearDown(void) {
// // clean stuff up here
// }

void obviouslyTrue(void){
     TEST_ASSERT_EQUAL(36, 36);
}

void test_bounding_box_printout(void) {
    Onboarding onboarder;
    onboarder.printGeographicBounds();
    TEST_ASSERT_TRUE(true);
}


void process() {
    UNITY_BEGIN();
    RUN_TEST(obviouslyTrue);
    RUN_TEST(test_bounding_box_printout);
    UNITY_END();
}

#ifdef ARDUINO

#include <Arduino.h>
void setup() {
    // NOTE!!! Wait for >2 secs
    // if board doesn't support software reset via Serial.DTR/RTS
    delay(500);
    process();
}

void loop() {
 
}

#else

int main(int argc, char **argv) {
    process();
    return 0;
}

#endif



#include <DisplayData.cpp>
#include <unity.h>

// void setUp(void) {
// // set stuff up here
// }

// void tearDown(void) {
// // clean stuff up here
// }
DisplayData data = DisplayData(0, 0, {3824}, {"J"}, {}, 0);;

void obviouslyTrue(void){
     TEST_ASSERT_EQUAL(36, 36);
}

void test_three_routes_zero_predictions(void) {
    String json = "{'copyright':'All data copyright Chapel Hill Transit 2021.','predictions':[{'routeTag':'G','stopTag':'airplong','routeTitle':'G','agencyTitle':'Chapel Hill Transit','dirTitleBecauseNoPredictions':'To UNC Hospital','stopTitle':'MLK Jr Blvd at Longview St'},{'routeTag':'NS','stopTag':'airplong','routeTitle':'NS','agencyTitle':'Chapel Hill Transit','dirTitleBecauseNoPredictions':'Southbound','stopTitle':'MLK Jr Blvd at Longview St'},{'routeTag':'HS','stopTag':'airplong','routeTitle':'HS','agencyTitle':'Chapel Hill Transit','dirTitleBecauseNoPredictions':'Smith Level Rd. at BPW Club','stopTitle':'MLK Jr Blvd at Longview St'}]}";
    data.parseAndFilterToArray(json);
    TEST_ASSERT_TRUE( data.getPredictions().size() == 0 );
    // TEST_ASSERT_EQUAL_STRING( "JN", data.getPredictions().at(1).routeTag.c_str());
    data.clearPredictions();
}

void test_two_routes_one_prediction(void) {
    String json = "{'predictions':[{'agencyTitle':'Chapel Hill Transit','routeTag':'N','routeTitle':'N','stopTitle':'Columbia at Longview','stopTag':'colulong_s','dirTitleBecauseNoPredictions':'To Family Practice Building'},{'agencyTitle':'Chapel Hill Transit','routeTag':'JN','routeTitle':'JN','direction':{'title':'To Rock Haven Rd','prediction':{'isDeparture':'false','minutes':'1000','seconds':'2181','tripTag':'1280','vehicle':'1716','affectedByLayover':'true','block':'651','dirTag':'2rockhaven','epochTime':'1545519845158'}},'stopTitle':'Columbia at Longview','stopTag':'colulong_s'}],'copyright':'All data copyright Chapel Hill Transit 2018.'}";
    data.parseAndFilterToArray(json);
    int minutes = data.getPredictions().at(0).minutes;
    TEST_ASSERT_EQUAL( 1000, minutes);
    TEST_ASSERT_EQUAL_STRING( "JN", data.getPredictions().at(0).routeTag.c_str());
    
    data.clearPredictions();
}

void test_two_routes_one_prediction2(void) {
    String json = "{'predictions':[{'agencyTitle':'Chapel Hill Transit','routeTag':'N','routeTitle':'N','stopTitle':'Columbia at Longview','stopTag':'colulong_s','dirTitleBecauseNoPredictions':'To Family Practice Building'},{'agencyTitle':'Chapel Hill Transit','routeTag':'JN','routeTitle':'JN','direction':{'title':'To Rock Haven Rd','prediction':{'isDeparture':'false','minutes':'16','seconds':'2181','tripTag':'1280','vehicle':'1716','affectedByLayover':'true','block':'651','dirTag':'2rockhaven','epochTime':'1545519845158'}},'stopTitle':'Columbia at Longview','stopTag':'colulong_s'}],'copyright':'All data copyright Chapel Hill Transit 2018.'}";
    data.parseAndFilterToArray(json);
    int minutes = data.getPredictions().at(0).minutes;
    TEST_ASSERT_EQUAL( 16, minutes);
    TEST_ASSERT_EQUAL_STRING( "JN", data.getPredictions().at(0).routeTag.c_str());
    data.clearPredictions();
}

void test_three_routes_three_predictions_on_one_route_only(void) {
    String json = "{'copyright':'All data copyright Chapel Hill Transit 2021.','predictions':[{'routeTag':'G','stopTag':'airplong','routeTitle':'G','agencyTitle':'Chapel Hill Transit','dirTitleBecauseNoPredictions':'To UNC Hospital','stopTitle':'MLK Jr Blvd at Longview St'},{'routeTag':'NS','stopTag':'airplong','routeTitle':'NS','agencyTitle':'Chapel Hill Transit','stopTitle':'MLK Jr Blvd at Longview St','direction':{'prediction':[{'affectedByLayover':'true','seconds':'890','tripTag':'119','minutes':'14','isDeparture':'false','block':'353','dirTag':'S','epochTime':'1629049066649','vehicle':'2004'},{'affectedByLayover':'true','seconds':'3144','tripTag':'127','minutes':'52','isDeparture':'false','block':'354','dirTag':'S','epochTime':'1629051320631','vehicle':'1907'},{'affectedByLayover':'true','seconds':'5544','tripTag':'120','minutes':'92','isDeparture':'false','block':'353','dirTag':'S','epochTime':'1629053720631','vehicle':'2004'}],'title':'Southbound'}},{'routeTag':'HS','stopTag':'airplong','routeTitle':'HS','agencyTitle':'Chapel Hill Transit','dirTitleBecauseNoPredictions':'Smith Level Rd. at BPW Club','stopTitle':'MLK Jr Blvd at Longview St'}]}";
    data.parseAndFilterToArray(json);
    TEST_ASSERT_EQUAL( 14, data.getPredictions().at(0).minutes);
    TEST_ASSERT_EQUAL( 52, data.getPredictions().at(1).minutes);
    TEST_ASSERT_EQUAL( 92, data.getPredictions().at(2).minutes);
    TEST_ASSERT_EQUAL_STRING( "NS", data.getPredictions().at(0).routeTag.c_str());
    TEST_ASSERT_EQUAL_STRING( "NS", data.getPredictions().at(1).routeTag.c_str());
    TEST_ASSERT_EQUAL_STRING( "NS", data.getPredictions().at(2).routeTag.c_str());
    data.clearPredictions();
}


void process() {
    UNITY_BEGIN();
    RUN_TEST(obviouslyTrue);
    RUN_TEST(test_three_routes_zero_predictions);
    RUN_TEST(test_two_routes_one_prediction);
    RUN_TEST(test_two_routes_one_prediction2);
    RUN_TEST(test_three_routes_three_predictions_on_one_route_only);
    UNITY_END();
}

#ifdef ARDUINO

#include <Arduino.h>
void setup() {
    // NOTE!!! Wait for >2 secs
    // if board doesn't support software reset via Serial.DTR/RTS
    delay(500);
    process();
}

void loop() {
 
}

#else

int main(int argc, char **argv) {
    process();
    return 0;
}

#endif



//case 2: multiple routes, single prediction per route


//Case 3 multiple routes, mixed prediction quantities


//case 4 multiple routes, no predictions


//Case 5 single route, multiple prediction


//Case 6 single route, single prediction

//Case 7 single route, no predictions



/* two routes no predictions 
    {"predictions":[{"agencyTitle":"Chapel Hill Transit","routeTag":"NS","routeTitle":"NS","stopTitle":"Martin Luther King Jr Blvd at Longview St","stopTag":"airplong","dirTitleBecauseNoPredictions":"Southbound"},{"agencyTitle":"Chapel Hill Transit","routeTag":"G","routeTitle":"G","stopTitle":"Martin Luther King Jr Blvd at Longview St","stopTag":"airplong","dirTitleBecauseNoPredictions":"To University Mall"}],"copyright":"All data copyright Chapel Hill Transit 2018."}
*/


/* 3 routes, one preduction
        {"predictions":[{"agencyTitle":"Chapel Hill Transit","routeTag":"A","routeTitle":"A","stopTitle":"Columbia at Longview","stopTag":"colulong_s","dirTitleBecauseNoPredictions":"To Martin Luther King Jr Blvd"},{"agencyTitle":"Chapel Hill Transit","routeTag":"N","routeTitle":"N","stopTitle":"Columbia at Longview","stopTag":"colulong_s","dirTitleBecauseNoPredictions":"To Family Practice Building"},{"agencyTitle":"Chapel Hill Transit","routeTag":"JN","routeTitle":"JN","direction":{"title":"To Rock Haven Rd","prediction":{"isDeparture":"false","minutes":"36","seconds":"2181","tripTag":"1280","vehicle":"1716","affectedByLayover":"true","block":"651","dirTag":"2rockhaven","epochTime":"1545519845158"}},"stopTitle":"Columbia at Longview","stopTag":"colulong_s"}],"copyright":"All data copyright Chapel Hill Transit 2018."}
*/


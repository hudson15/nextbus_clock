﻿export interface agencyspecificroutesterse {
  copyright: string;
  route?: (RouteEntity)[] | null;
}
export interface RouteEntity {
  lonMax: string;
  color: string;
  oppositeColor: string;
  stop?: (StopEntity)[] | null;
  tag: string;
  latMin: string;
  title: string;
  latMax: string;
  lonMin: string;
  direction?: (DirectionEntity)[] | null;
}
export interface StopEntity {
  stopId?: string | null;
  lon: string;
  tag: string;
  shortTitle?: string | null;
  title: string;
  lat: string;
}
export interface DirectionEntity {
  stop?: (StopEntity1)[] | null;
  name: string;
  useForUI: string;
  tag: string;
  title: string;
}
export interface StopEntity1 {
  tag: string;
}

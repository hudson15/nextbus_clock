
export interface ESP8266FlashSettings {
    Displays: Displays;
  }
  export interface Displays {
    0: IndividualDisplay;
    1?: IndividualDisplay;
  }
  export interface IndividualDisplay {
    Location: string;
    StopIDs: (number)[] | null;
    BadBusRoutes: (string)[] | null;
    BadDirTags : (string)[] | null;
    TravelTimeMin: number;
    showRouteTagInterval: number;
  }
  
//none of this has been verified
let existingConfig = {
  "0": {
    "StopIDs": [3450, 2825],
    "BadBusRoutes": [],
    "BadDirTags": [],
    "TravelTimeMin": 0,
    "showRouteTagInterval": 1,
  },
};


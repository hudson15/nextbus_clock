﻿//This interface either has a
// root.predictions[0].direction["prediciton"] array of predictions,  OR a
// root.predictions[0].dirTitleBecauseNoPredictions


export interface stopspecific {
  copyright: string;
  predictions?: (PredictionsEntity)[] | Predictions | null;
}

export interface Predictions {
  routeTag: string;
  stopTag: string;
  routeTitle: string;
  agencyTitle: string;
  dirTitleBecauseNoPredictions: string;
  stopTitle: string;
}

export interface PredictionsEntity {
  routeTag: string;
  stopTag: string;
  routeTitle: string;
  agencyTitle: string;
  stopTitle: string;
  direction?: Direction | null;
  dirTitleBecauseNoPredictions?: string | null;
}
export interface Direction {
  prediction?: (PredictionEntity)[] | null;
  title: string;
}
export interface PredictionEntity {
  seconds: string;
  tripTag: string;
  minutes: string;
  isDeparture: string;
  block: string;
  dirTag: string;
  epochTime: string;
  vehicle: string;
  affectedByLayover?: string | null;
}







export interface InternalStop {
    lat:number,
    lon:number, 
    stopId:string
}

export interface InternalRoute {
    routeTag: string, 
    direction_full: string,
}

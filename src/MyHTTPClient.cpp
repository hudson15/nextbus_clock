#include <Arduino.h>
#include <MyHTTPClient.h>
#include <ArduinoJson.h>
struct APIException : public std::exception
{
	const char * what () const throw ()
    {
    	return "C++ Exception";
    }
};

String MyHTTPClient::httpGetJsonForStop(String agency, int StopID){

    const uint8_t fingerprint[20] = {111,226,161,144,103,138,198,219,27,69,0,0,188,129,191,136,162,49,119,11};
    std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);

    client->setFingerprint(fingerprint);

    HTTPClient https;
    // const String baseRequest = "http://webservices.nextbus.com/service/publicJSONFeed?command=predictions&a=chapel-hill&stopId="; //depreciated 7/17/2021
     const String baseRequest = "https://retro.umoiq.com/service/publicJSONFeed?command=predictions&a=" + agency + "&stopId=";           //New 

    https.begin(*client, baseRequest + String(StopID));
    Serial.printf("\nGET stopID %i | RETURNS http: ", StopID);
    int httpCode = https.GET();
    Serial.println(httpCode);

    String json = https.getString();
    https.end();
    if(httpCode>=200 && httpCode <300){
      return json;  
    }
    else{
         return String("Http code not in successful range");
    }
    
}

// String MyHTTPClient::getAgencyList(){
//     //Returns all agencies served by the api
//     const uint8_t fingerprint[20] = {111,226,161,144,103,138,198,219,27,69,0,0,188,129,191,136,162,49,119,11};
//     std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);

//     client->setFingerprint(fingerprint);

//     HTTPClient https;
//     const String baseRequest = "https://retro.umoiq.com/service/publicJSONFeed?command=agencyList";
//     Serial.println(baseRequest);
//     https.begin(*client, baseRequest);
//     int httpCode = https.GET();
//     String json = https.getString();
//     https.end();
//     if(httpCode>=200 && httpCode <300){
//       return json;  
//     }
//     else{
//          return String("Agency list call - Http code not in successful range. CODE:" + String(httpCode) + " . JSON:" + json);
//     }
    
// }

// String MyHTTPClient::getRouteList(String agency){
//     //Returns all bus-routes (lines) of a particular agency
//     const uint8_t fingerprint[20] = {111,226,161,144,103,138,198,219,27,69,0,0,188,129,191,136,162,49,119,11};
//     std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);
    
//     client->setFingerprint(fingerprint);

//     HTTPClient https;
//     const String baseRequest = "https://retro.umoiq.com/service/publicJSONFeed?command=routeList&a="+ agency;
//     Serial.println(baseRequest);
//     https.begin(*client, baseRequest);
//     int httpCode = https.GET();
//     String json = https.getString();
    
    
//     if(httpCode>=200 && httpCode <300){
//       return json;  
//     }
//     else{
//          return String("Route list call - Http code not in successful range. CODE:" + String(httpCode) + " . JSON:" + json);
//     }
    
// }

// String MyHTTPClient::getRouteConfig(String agency, String route_tag){
//     //Get information about a particular route from a particular agency, including latatude and longitude of stops. pass blank route tag to return all
    
//     Serial.print(F("preclient:"));
//     Serial.println(ESP.getFreeHeap()); // 38304

//     const uint8_t fingerprint[20] = {111,226,161,144,103,138,198,219,27,69,0,0,188,129,191,136,162,49,119,11};
//     std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);
   
//     client->setFingerprint(fingerprint);

//     Serial.print(F("prehttp:"));
//     Serial.println(ESP.getFreeHeap()); // 31808
//     HTTPClient https;
//      https.setFollowRedirects(HTTPC_FORCE_FOLLOW_REDIRECTS); //TODO
//     //if we are going to use the stream we can not use chunked transfer encoding
//     https.useHTTP10(true); 
//     String baseRequest = "https://retro.umoiq.com/service/publicJSONFeed?command=routeConfig&a=";
//     baseRequest += agency;
//     baseRequest += "&r=";
//     baseRequest += route_tag;
//     baseRequest += "&terse";
    
//      Serial.print(F("prebegin:"));
//     Serial.println(ESP.getFreeHeap()); // 31664

//     https.begin(*client, baseRequest);
//     Serial.print(F("postbegin:"));
//     Serial.println(ESP.getFreeHeap()); // 31552
//     int httpCode = https.GET();
//     Serial.println(baseRequest);
//     Serial.println(httpCode);
//      Serial.print(F("preend:"));
//     Serial.println(ESP.getFreeHeap()); // 10784
    
  
    
    
//     Serial.print(F("postend:"));
//     Serial.println(ESP.getFreeHeap()); //10784
//     if(httpCode>=200 && httpCode <300){


//     DynamicJsonDocument doc(3000);
//     StaticJsonDocument<128> filter;
//     filter["route"]["latMax"] = true;
//     filter["route"]["latMin"] = true;
//     filter["route"]["lonMax"] = true;
//     filter["route"]["lonMin"] = true;
//     DeserializationError error = deserializeJson(doc, https.getStream(), DeserializationOption::Filter(filter));
//      if (error) {
//     Serial.print(F("deserializeJson() failed: "));
//     Serial.println(error.f_str());
//     return "F";
//     }
//     https.end();
//     String output;
//     serializeJson(doc, output);
//       return output;  
//     }
//     else{
//         return String("RouteConfig call - Http code not in successful range");
//     }
    
// }

#include <DisplayData.h>
#include <ArduinoJson.h>
#include<Prediction.h>
#include<MyHttpClient.h>


//Constructor with "member list" for initialization
DisplayData::DisplayData(int displayAddress, int travelTimeToStop, std::vector<int> stopIDs, std::vector<String> BadRouteTags, std::vector<String> BadDirTags, int showRouteTagsInterval) : 
displayAddress(displayAddress), 
travelTimeToStop(travelTimeToStop),
stopIDs(stopIDs),
BadRouteTags(BadRouteTags),
BadDirTags(BadDirTags),
showRouteTagsInterval(showRouteTagsInterval)
{
  //Constructor method cont...
  this->print();
}

//Prints results (useful for debugging)
void DisplayData::print() { 
  
  Serial.printf("\nInit Display %i with TTS of %i |",displayAddress,travelTimeToStop);
  Serial.print(F(" StopIDS: "));
  for (auto value : stopIDs) {
    Serial.print(value);
    Serial.print(",");
  }
  Serial.print(F("| Ignoring Routes: "));
  for (auto value : BadRouteTags) {
    Serial.print(value);
    Serial.print(",");
  }
  Serial.print("\n");

  //End debug
    return ;
}

//Getters
int DisplayData::getDisplayAddress(){return displayAddress;} 
std::vector<int> DisplayData::getStopIDs(){return stopIDs;}
int DisplayData::getTravelTimeToStop(){return travelTimeToStop;}
std::vector<Prediction> DisplayData::getPredictions(){return predictions;}
std::vector<String> DisplayData::getBadRouteTags(){return BadRouteTags;}
std::vector<String> DisplayData::getBadDirTags(){return BadDirTags;}
int DisplayData::getRouteTagInterval(){return showRouteTagsInterval;}

//Coordinates the retreival and postprocessing of predictions
 void DisplayData::updatePredictions(){

   MyHTTPClient http ;
   for(int stopID : stopIDs){
     parseAndFilterToArray(http.httpGetJsonForStop("chapel-hill", stopID));
   }

  sortPredictions(); 

  //Prints results
    Serial.printf("Displaying %i total predictions \n", predictions.size());
    for( Prediction &prediction: predictions){
        Serial.println(prediction.minutes);
    }
   
}




//Parses the JSON string to extract ALL predictions into parsed vector




//Several cases all with different paths.. parser Optimized for execution speed, not readability

//One busLine, multiple predictions            minutesTillArrival = root["predictions"]["direction"]["prediction"][busNumber]["minutes"].as<String>();
//One busline, one prediction                 minutesTillArrival = root["predictions"]["direction"]["prediction"]["minutes"];

//multiple buslines, multiple predictions      minutesTillArrival = root["predictions"][busLine]["direction"]["prediction"][busNumber]["minutes"].as<String>();
//multiple buslines, one prediction           minutesTillArrival = root["predictions"][busLine]["direction"]["prediction"]["minutes"].as<String>();

void DisplayData::parseAndFilterToArray(String JsonString){
 // Serial.println(JsonString);
  const size_t bufferSize = JSON_ARRAY_SIZE(2) + JSON_ARRAY_SIZE(5) + 3*JSON_OBJECT_SIZE(2) + 2*JSON_OBJECT_SIZE(6) + JSON_OBJECT_SIZE(8) + 5*JSON_OBJECT_SIZE(9)+1178;
  DynamicJsonDocument doc(bufferSize);
  deserializeJson(doc, JsonString);
  bool onlyOneRoute;
  
  //This runs if the HTML request was successful, but there was no data. Ex: if stopid is wrong
  if(doc["Error"]){
    Serial.println(F("ERROR HTTP body:"));
    Serial.println(doc["Error"].as<String>());
    return;
  }

  if( !(doc["predictions"]) ){
    Serial.println(F("ERROR no predictions object, check and make sure the json is formatted correctly"));
  
    return;
  }

  Serial.println(F("Tag: \tMin: \tGroup: "));                                       //Print route Tag
  Serial.println(F("---------------------"));



  //Traverse busLine by busLine (A, G, NS, ETC...)
  for(int busLine = 0; busLine<10; busLine++){
          
    String routeTag=doc["predictions"][busLine]["routeTag"].as<String>();  //Common case is multiple bus lines, try to get first line
    if(routeTag=="null"){
      routeTag = doc["predictions"]["routeTag"].as<String>();     //if null, Perhaps there is only a single line?
      if(routeTag=="null"){
        break;                                                    //Still null? there must be no predictions in the string
      }else{                                                        
        onlyOneRoute=true;                                         //Single line found, take note of this for faster calculations later this is an edge case
        busLine=10;
      }
    }else{
      onlyOneRoute=false;
    }


    //EDGE CASE, if route only expects a single bus 
    if(onlyOneRoute){
      String singleRouteSinglePrediction = doc["predictions"]["direction"]["prediction"]["minutes"].as<String>();
      String singleRouteSingleDirTag = doc["predictions"]["direction"]["prediction"]["dirTag"].as<String>();
      if(singleRouteSinglePrediction!="null"){
        Serial.print("SINGLE ROUTE SINGLE PREDICTION: ");
        Serial.println(String(singleRouteSinglePrediction));
        addPrediction(Prediction{.minutes = singleRouteSinglePrediction.toInt(), .routeTag = routeTag, .dirTag = singleRouteSingleDirTag});
        break; 
      }  
    }

    //Gather all available predictions for the current bus line, exit when out of data
    for(int busNumber=0;busNumber<10; busNumber++){

      String minutesTillArrival;
      String dirTag;      
      if(onlyOneRoute){
        minutesTillArrival = doc["predictions"]["direction"]["prediction"][busNumber]["minutes"].as<String>();
        dirTag = doc["predictions"]["direction"]["prediction"][busNumber]["dirTag"].as<String>();
      }else{
        minutesTillArrival = doc["predictions"][busLine]["direction"]["prediction"][busNumber]["minutes"].as<String>();
        dirTag = doc["predictions"]["direction"]["prediction"][busNumber]["dirTag"].as<String>();

        if(minutesTillArrival=="null"){
                minutesTillArrival = doc["predictions"][busLine]["direction"]["prediction"]["minutes"].as<String>(); //Multiple routes, one of them has only a single prediction
                dirTag = doc["predictions"]["direction"]["prediction"][busNumber]["dirTag"].as<String>();
                busNumber=10;
              }
      }

      if(!(minutesTillArrival=="null")){  
        Prediction p = {.minutes = minutesTillArrival.toInt(), .routeTag = routeTag, .dirTag = dirTag};                                                               
        addPrediction(p);
      }else{         
        break;                                                  //We have parsed every bus for this route, no need to continue
      }              
    }   
  }        
}



void DisplayData::sortPredictions(){
     std::sort(predictions.begin(),predictions.end());
} 

void DisplayData::addPrediction(Prediction predictionInstance){
   bool filter = shouldFilter(predictionInstance);
   Serial.printf("%s \t %i \t ", predictionInstance.routeTag.c_str(), predictionInstance.minutes);
   Serial.printf("%s \n", filter ? "Filtered" : "Added");          
   if (!filter){
    predictions.push_back(predictionInstance);
   }
}

//This method keeps predictions from getting parsed which do not fulfil some set of conditions
bool DisplayData::shouldFilter(Prediction predictionInstance){
  
  if(predictionInstance.minutes < travelTimeToStop){
    return true;
  }
  else if(std::find(BadRouteTags.begin(), BadRouteTags.end(), predictionInstance.routeTag) != BadRouteTags.end()){
    return true;
  }else if(std::find(BadDirTags.begin(), BadDirTags.end(), predictionInstance.dirTag) != BadDirTags.end()){
    return true;
  }
  else{
    return false;
  }

}

void DisplayData::clearPredictions(){
  predictions.clear();
}

void DisplayData::addStopID(int stopID){
  Serial.printf("*Display %i :",getDisplayAddress());
  Serial.printf(" adding Stop %i \n",stopID);
  stopIDs.push_back(stopID);
}
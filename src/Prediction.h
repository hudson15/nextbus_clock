
#ifndef PREDICTION_H
#define PREDICTION_H

struct Prediction {

    int minutes;
    String routeTag;
    String dirTag;
    enum group {FILTERED = 0, STANDARD = 1 };

    //allows sorting via standard sort by implementing the < operator

    bool operator < (const Prediction& str) const
    {
        return (minutes < str.minutes);
    }
} ;

#endif 


#include <Arduino.h>
#include<String>
#include<vector>
#include<Prediction.h>

#define MAX_PREDICTIONS_TO_PROCESS 40
#define MAX_PREDICTIONS_TO_DISPLAY 10

//DATA class for a SINGLE display.
//This class is meant to retreive, store, and manipulate the data which will be used for a SINGLE display
class DisplayData{

    public:
    DisplayData(int displayAddress = 0, int travelTimeToStop = 0, std::vector<int> stopIDs ={} ,std::vector<String> BadRouteTags = {}, std::vector<String> BadDirTags = {}, int showRouteTagsInterval = 0);
    std::vector<Prediction> predictions {};

    void print();
    //Use this as FSM logic to determine if processing or not
    int status = 0;
    
    // 0: parsing
    // 1: successful parse
    // 2: no wifi
    // 3: http error (error code)
    // 4:

    //GettersandSetters
    int getDisplayAddress(); 
    std::vector<int> getStopIDs();
    void addStopID(int stopID);
    int getTravelTimeToStop();
    std::vector<Prediction> getPredictions();
    std::vector<String> getBadRouteTags();
    std::vector<String> getBadDirTags();
    int getRouteTagInterval();
    
    
    
    //The stop IDS to aggregate together onto this display
    
    void updatePredictions();

    //these used to be private but were moved for unit testing
    void parseAndFilterToArray(String JsonString);
    void clearPredictions();

    private:
    const int displayAddress;
    std::vector<int> stopIDs;
    int travelTimeToStop = 1;
    std::vector<String> BadRouteTags;
    std::vector<String> BadDirTags;
    int showRouteTagsInterval;
    //helper
    
    void sortPredictions(); 
    void addPrediction(Prediction prediction);
    bool shouldFilter(Prediction prediction);

    
    //pull in all the stopIDs
        //filter out any unwanted routes (Should be done on parsing)
        //map just the minutes into a new array
        //sort minutes array
};
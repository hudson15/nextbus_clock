#include "DisplayView.h"
#include<Prediction.h>

#define dataPin D4
#define csPin D3
#define clockPin D2


DisplayView::DisplayView(int numDisplays, int digitsPerDisplay)
  : _ledControlInstance(LedControl(dataPin,clockPin,csPin,numDisplays)),
    _numDisplays(numDisplays),
    _digitsPerDisplay(digitsPerDisplay){}


int digitsInInteger(int n)
{
    return n > 0 ? (int) log10 ((double) n) + 1 : 1;
}

void DisplayView::displayPrediction(int displayAddr, const std::vector<Prediction> &predictions, long msToDisplayRouteTags){
  _ledControlInstance.shutdown(displayAddr,false);
  if(msToDisplayRouteTags>0){
    printRouteTags(displayAddr, predictions);
    delay(msToDisplayRouteTags);
  }
  printNumbersWithSpacesInBoundsofDisplay(displayAddr, predictions);
}


//TODO consolidate this and print unsigned. With the updated character table this method is suitable for displaying digits also.
void DisplayView::printRouteTags(int displayAddr,const std::vector<Prediction> &predictions){
  _ledControlInstance.clearDisplay(displayAddr);
  int nextDigitIndex =_digitsPerDisplay-1;

  for(Prediction prediction : predictions) {
    if(prediction.FILTERED){
      continue;
    }
    int lengthOfPrediction = prediction.routeTag.length();    //More information
    //int lengthOfPrediction = digitsInInteger(prediction.minutes); //cleaner
    
    //Serial.printf("%s ",prediction.routeTag.c_str());
    if(nextDigitIndex - lengthOfPrediction >= -1){

      switch(prediction.routeTag.length()){
        case 1: 
        _ledControlInstance.setChar(displayAddr,nextDigitIndex, prediction.routeTag.charAt(0),true); //Will always write the first route tag on the far left because its initialized to 2
        break;

        case 2:
         _ledControlInstance.setChar(displayAddr,nextDigitIndex, prediction.routeTag.charAt(0),false);
         _ledControlInstance.setChar(displayAddr,nextDigitIndex-1, prediction.routeTag.charAt(1),true);
         break;

         default:
         _ledControlInstance.setChar(displayAddr,nextDigitIndex, prediction.routeTag.charAt(0),false);
         _ledControlInstance.setChar(displayAddr,nextDigitIndex-1, prediction.routeTag.charAt(1),true);
 
      }

      nextDigitIndex -= lengthOfPrediction;
      nextDigitIndex--; //add space before next 
    }else{
      // break; //break when the next digit to be printed will overflow the display
    }
  }
}
void DisplayView::displayError(int displayAddr, byte errCode){
  //0: Wifi not connected, N/A
  //1: Http error, code
  _ledControlInstance.clearDisplay(displayAddr);
  _ledControlInstance.setChar(displayAddr,_digitsPerDisplay-1,'e',false);
  _ledControlInstance.setDigit(displayAddr,_digitsPerDisplay-2,errCode, false);

}

void  DisplayView::setBrightness(unsigned brightness){
  for(int address=0; address<_numDisplays;address++){
    _ledControlInstance.setIntensity(address,brightness);
  }
}
void DisplayView::disableDisplay(int displayAddr){
    _ledControlInstance.shutdown(displayAddr,true);
}

//prints a nuumber between 0 and 999 beginning at startindex, returns position of next char 
unsigned DisplayView::printUnsigned(int displayAddr, byte startIndex, int numberToPrint) {
    
    //setup numbers and extract individual digits
    int ones;
    int tens;
    int hundreds; 
    
    unsigned length = 1;

    ones = numberToPrint % 10;
    numberToPrint = numberToPrint / 10;
    tens = numberToPrint % 10;
    numberToPrint = numberToPrint / 10;
    hundreds = numberToPrint;     
    
    //Now print the number digit by digit (INDEX 0 is FAR RIGHT) unusual
    int nextCharIndex = _digitsPerDisplay-startIndex;
    //now index 0 is far left, decrement index with every print
    
    if(hundreds){
      _ledControlInstance.setDigit(displayAddr,nextCharIndex--,(byte)hundreds,false);
      length=3;
    }
    if(tens){
      _ledControlInstance.setDigit(displayAddr,nextCharIndex--,(byte)tens,false);
      length=2;
    }
    _ledControlInstance.setDigit(displayAddr,nextCharIndex--,(byte)ones,false);

    return nextCharIndex;
}


 

//right to left printing, fits as many numbers as possible in display
void DisplayView::printNumbersWithSpacesInBoundsofDisplay(int displayAddr, const std::vector<Prediction> &predictions){
  
  _ledControlInstance.clearDisplay(displayAddr);

  //iterate through the sorted vector 

  int nextDigitIndex =1;

  for(Prediction prediction : predictions) {
    if(prediction.FILTERED){
      continue;
    }
    int lengthOfPrediction = digitsInInteger(prediction.minutes);
    //Serial.printf("Display %i  |  nextDigitIndex %i  |  prediction %i  | len %i \n", displayAddr, nextDigitIndex, prediction, lengthOfPrediction);
   

     /*Tests digitsInInteger;
    for(int i=1;i<9;i++){
      Serial.print(i);
      Serial.print(":");
      Serial.println(digitsInInteger(42));
    }*/
    
    //for some reason these are indexed to start at one, consider refactoring
    if(nextDigitIndex + lengthOfPrediction <= _digitsPerDisplay+1){
      printUnsigned(displayAddr, nextDigitIndex, prediction.minutes);
      nextDigitIndex += lengthOfPrediction;
      nextDigitIndex++; //add space before next 
    }else{
      // break; //break when the next digit to be printed will overflow the display
    }
  }
}
  void DisplayView::displayIp(String ip){
    Serial.printf("displaying IP: %s", ip.c_str());
    _ledControlInstance.shutdown(0,false);

  
    _ledControlInstance.setChar(0,7,ip.charAt(0),false);
    _ledControlInstance.setChar(0,6,ip.charAt(1),false);
    _ledControlInstance.setChar(0,5,ip.charAt(2),true);
    _ledControlInstance.setChar(0,4,ip.charAt(4),false);
    _ledControlInstance.setChar(0,3,ip.charAt(5),false);
    _ledControlInstance.setChar(0,2,ip.charAt(6),true);
    _ledControlInstance.setChar(0,1,ip.charAt(8),true);
    _ledControlInstance.setChar(0,0,ip.charAt(10),false);
    _ledControlInstance.shutdown(0,false);
    
   

  }

  void DisplayView::flashText(const char* text){
    Serial.printf("displaying text word by word: %s\n", text);
    
    for(int charPosition, physicalPosition=0; physicalPosition<_digitsPerDisplay; charPosition++, physicalPosition++){

      bool doWeDecimal = (charPosition+1 < _digitsPerDisplay && text[charPosition+1]=='.');
      
      

      if(text[charPosition]==_NULL){
         _ledControlInstance.setChar(0,_digitsPerDisplay-physicalPosition-1, ' ', doWeDecimal); //Fill null space
      }
      else{_ledControlInstance.setChar(0,_digitsPerDisplay-physicalPosition-1,text[charPosition],doWeDecimal);
      }
      if(doWeDecimal){charPosition++;}
    }
    
    _ledControlInstance.shutdown(0,false);
  }

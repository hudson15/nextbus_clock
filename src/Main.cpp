
#include <FS.h> //this needs to be first, or it all crashes and burns...
#include <vector>
#include <sstream>
#include<DisplayData.h>
#include<DisplayView.h>
#include <WiFiManager.h> 
#include <ArduinoJson.h>
#include <string.h>     

#define NUMBER_OF_DISPLAYS 1
#define DIGITS_PER_DISPLAY 8


std::vector<DisplayData> Displays;
DisplayView view = DisplayView(NUMBER_OF_DISPLAYS, DIGITS_PER_DISPLAY);


//flag for saving data
bool wifiSaveConfig = false;
bool firstRun = false;


String IpAddress2String(const IPAddress& ipAddress)
{
  return String(ipAddress[0]) + String(".") +\
  String(ipAddress[1]) + String(".") +\
  String(ipAddress[2]) + String(".") +\
  String(ipAddress[3])  ; 
}

//callback which is launched anytime WIFI Config page is entered
void configModeCallback (WiFiManager *myWiFiManager) {
  Serial.println(F("Entered config mode \n"));
  view.flashText("SEtUp");
  //view.flashText("Con.2 Y.Fi");
  //view.displayIp(IpAddress2String(WiFi.softAPIP()));
}

//callback which is launched anytime WIFI Config page sends new data
void saveConfigCallback () {
  Serial.print(F("New Settings received via GET\n"));
  wifiSaveConfig = true;
}

//adds or overwrites the appropriate entry in "./config.json"
boolean jsonWriteDisplay(DisplayData displayToWrite){
  //TODO: the buffer size could actually be computed to the exact bytes using info from displayToWrite.
  // However, for rapid development a dynamic buffer will be just fine.
  Serial.printf("Updating Json for Display %i\n",displayToWrite.getDisplayAddress());
  const size_t capacity = 2*JSON_ARRAY_SIZE(0) + JSON_ARRAY_SIZE(1) + JSON_ARRAY_SIZE(2) + JSON_OBJECT_SIZE(2) + 2*JSON_OBJECT_SIZE(3);
      DynamicJsonDocument doc(capacity);

      //build the json structure for a single display
      JsonObject root = doc.as<JsonObject>();
      JsonObject displaySettings = root.createNestedObject(String(displayToWrite.getDisplayAddress()));
      JsonArray displaySettings_StopIDs = displaySettings.createNestedArray("StopIDs");
      JsonArray displaySettings_BadBusRoutes = displaySettings.createNestedArray("BadBusRoutes");
      JsonArray displaySettings_BadDirTags = displaySettings.createNestedArray("BadDirTags");

        for (int& i : displayToWrite.getStopIDs()){
          displaySettings_StopIDs.add(i);
        }

        for (String& i : displayToWrite.getBadRouteTags()){
          displaySettings_BadBusRoutes.add(i);
        }

        for (String& i : displayToWrite.getBadDirTags()){
          displaySettings_BadDirTags.add(i);
        }
      
      displaySettings["TravelTimeMin"] = displayToWrite.getTravelTimeToStop();
      displaySettings["showRouteTagInterval"] = displayToWrite.getRouteTagInterval();

    File configFile = SPIFFS.open("/config.json", "w");
    if (!configFile) {
      Serial.println(F("failed to open config file for writing"));
    }
    
    serializeJson(doc,configFile);
    configFile.close();
  return true;
}



//Mounts filesystem and reads config.json into a DisplayData object.
DisplayData jsonReadDisplay(int displayAddress){

 std::vector<int> stopIDs {};
 int travelTimeToStop = 1;
 std::vector<String> BadRouteTags {};
 std::vector<String> BadDirTags {};
 int showRouteTagInterval = 1;


 Serial.println(F("mounting FS..."));

  if (SPIFFS.begin()) {
    Serial.println(F("mounted file system"));
    if (SPIFFS.exists("/config.json")) {
      //file exists, reading and loading
      Serial.println(F("Reading config file"));
      File configFile = SPIFFS.open("/config.json", "r");
      if (configFile) {
        Serial.println(F("Config contains:"));
        Serial.print("\t");

        // Allocate a buffer to store contents of the file.
        size_t size = configFile.size();
        std::unique_ptr<char[]> buf(new char[size]);
        configFile.readBytes(buf.get(), size);
        DynamicJsonDocument doc(size*2);
        deserializeJson(doc, buf.get());
        serializeJson(doc, Serial);

        if (!(doc.isNull())) {

          String DisplayAddressStr = String(displayAddress);
          travelTimeToStop = doc[DisplayAddressStr]["TravelTimeMin"];
          showRouteTagInterval = doc[DisplayAddressStr]["showRouteTagInterval"];

          for(int i=0; i<5;i++){

            int stopToAdd = doc[DisplayAddressStr]["StopIDs"][i];
            if(stopToAdd) stopIDs.push_back(stopToAdd);

            String badRouteToAdd = doc[DisplayAddressStr]["BadBusRoutes"][i];
            if(badRouteToAdd!="null") BadRouteTags.push_back(badRouteToAdd);
            
            String badDirToAdd = doc[DisplayAddressStr]["BadDirTags"][i];
            if(badDirToAdd!="null") BadDirTags.push_back(badDirToAdd);
          }          
        }else {
          Serial.println(F("failed to load json config"));
        }
      }
    }else{
        Serial.println(F("No config file found"));
        firstRun = true;
      }
  } else {
    Serial.println(F("failed to mount FS"));
  }
  //end read

  return DisplayData(displayAddress, travelTimeToStop, stopIDs, BadRouteTags, BadDirTags, showRouteTagInterval);
}

 
//Parses input from GET request - consider basic input sanatization and feedback
DisplayData wifiReadNewDisplaySettings(WiFiManagerParameter& displayAddressParam, WiFiManagerParameter& travelTimeParam, WiFiManagerParameter& stopIDParam, WiFiManagerParameter& filteredRoutesParam,WiFiManagerParameter& filteredDirTagsParam, WiFiManagerParameter& showRouteTagsIntervalParam){
  
      int displayAddress = atoi(displayAddressParam.getValue());
      int travelTimeToStop = atoi(travelTimeParam.getValue());
      char* ids = strdup(stopIDParam.getValue());
      char* Xroutes = strdup(filteredRoutesParam.getValue());
      int showRouteTagInterval = atoi(showRouteTagsIntervalParam.getValue());

      std::vector<int> stopIDs {};
      std::vector<String> BadRouteTags {};
      std::vector<String> BadDirTags {};
      const char delimitor[5] = ", -.";

      //Parses IDS by delimitor
      char *idPointer = strtok(ids, delimitor);
      while (idPointer != NULL){
        Serial.printf ("wifi ID %s\n",idPointer);
        stopIDs.push_back(atoi(idPointer));
        idPointer= strtok (NULL, delimitor);
      }

      //Parses Routes by delimitor
      char *routesPointer = strtok(Xroutes, delimitor);
      while (routesPointer != NULL){
        Serial.printf ("wifi BadRouteTag %s\n",routesPointer);
        BadRouteTags.push_back(routesPointer);
        routesPointer= strtok (NULL, delimitor);
      }

      //Parses direction Tags by delimitor
      char *dirTagPointer = strtok(Xroutes, delimitor);
      while (dirTagPointer != NULL){
        Serial.printf ("wifi BadDirTag %s\n",dirTagPointer);
        BadDirTags.push_back(dirTagPointer);
        dirTagPointer= strtok (NULL, delimitor);
      }
      
      
  return DisplayData(displayAddress, travelTimeToStop, stopIDs, BadRouteTags, BadDirTags, showRouteTagInterval);
}



void setup(){
    Serial.begin(9600);
    
    
    //SPIFFS.format();  //clean FS, for testing
    WiFiManagerParameter displayAddressParam("DisplayAddress", "Display Address (zero index)", "0",5);
    WiFiManagerParameter travelTimeParam("TravelTimeToStop", "Ignore arrivals < X min", "",5);
    WiFiManagerParameter stopIDParam("stopIDS", "stopIDs:\"3450,2825\"", "",50);
    WiFiManagerParameter filteredRoutesParam("filteredRoutes", "Ignore Routes: \"A,T,NS\"", "",50);
    WiFiManagerParameter filteredDirTagsParam("filteredDirTags", "Ignore DirTags: \"oldseast\"", "",50);
    WiFiManagerParameter showRouteTagInterval("showRouteTagInterval", "Show route tag every X sec (0=never)", "",5);
    WiFiManager wifiManager;
    
    wifiManager.setSaveConfigCallback(saveConfigCallback);
    wifiManager.setAPCallback(configModeCallback);
    wifiManager.setConfigPortalTimeout(180);

    //Inject custom CSS and HTML into wifi setup page
    wifiManager.setCustomHeadElement("<style>#green {color:green;}</style>");
    String deviceMac = WiFi.macAddress();
    Serial.printf("MAC address: %s",deviceMac.c_str());
    String macP = "<p id='green'>MAC address: "+deviceMac+" (for UNC-PSK)</p>";
    WiFiManagerParameter MAC_Display_p(macP.c_str());

    String displayP = "<p>Displays: <span id='green'>"+String(NUMBER_OF_DISPLAYS)+"</span></p>";
    WiFiManagerParameter MAC_Display_p2(displayP.c_str());

    wifiManager.addParameter(&MAC_Display_p);
    wifiManager.addParameter(&MAC_Display_p2);


    //TODO test a loop here which enables multi display config.
    wifiManager.addParameter(&travelTimeParam);
    wifiManager.addParameter(&stopIDParam);
    wifiManager.addParameter(&filteredRoutesParam);
    wifiManager.addParameter(&filteredDirTagsParam);
    wifiManager.addParameter(&showRouteTagInterval);
    
    
    //Initilize displays
    for(int displayAddress=0;displayAddress<NUMBER_OF_DISPLAYS; displayAddress++){
      DisplayData representationOfSingleDisplay = jsonReadDisplay(displayAddress);

      //Launches config portal if any displays are unaccounted for in settings
      if(representationOfSingleDisplay.getStopIDs().empty()){
        Serial.printf("No configuration for display %i, Please configure from AP\n", displayAddress);
        wifiManager.startConfigPortal("BusClock");
        break;
      }else{
         Displays.push_back(representationOfSingleDisplay);
      }
    }

    //Debugging settings, setting both to true will emulate a clean startup with no filesystem
    wifiManager.setDebugOutput(true);
    wifiManager.setConnectTimeout(20);
    bool debug = false;
    if(debug){
      wifiManager.resetSettings();
      wifiManager.autoConnect("BusClock");
    }
    else{
      wifiManager.autoConnect("BusClock");
    }

  //Updates RAM and .json config with new display settings
  if (wifiSaveConfig) {
      Serial.print(F("Importing new configuration from wifi\n"));
      //Remove all existing displays
      Displays.clear();
      //Initialize a new one from GET request
      DisplayData d = wifiReadNewDisplaySettings(displayAddressParam, travelTimeParam, stopIDParam, filteredRoutesParam, filteredDirTagsParam, showRouteTagInterval);
      Displays.push_back(d);
      //(over)write display settings to .JSON
      jsonWriteDisplay(d);
  }
  

 
   // MLKID = 3450;
  // MLK T route only = 2825;
  // ColumbiaID = 4001;


}



void loop(){
  const int timeBetweenRefreshes = 60 * 1000;


//update and render predictions for each display
for(int address=0;address<NUMBER_OF_DISPLAYS; address++){

    DisplayData data = Displays.at(address);
    data.updatePredictions();
    /*Serial.printf("Predictions for Display: %i : Stop IDs" ,address);
    for (auto id : data.getStopIDs()) // access by reference to avoid copying
    {Serial.print(id+ ", ");}
    */
    //Display either an error, nothing, or the predictions
    if(data.status!=0){                             //Error
        Serial.print(F("ERROR"));
        view.displayError(address, data.status);
    }else if(data.getPredictions().size() == 0){    //Disable display
        Serial.print(F("No Predictions for Display "));
        Serial.println(address);
        view.disableDisplay(address);
        delay(timeBetweenRefreshes);
    }else{                                          //Main functionality, cycles between routeTags and predictions
        
        const int displayRoutesEveryXMS = data.getRouteTagInterval() * 1000;
        const int displayRoutesForXMs = 1 * 1000;

        int timeSinceRefresh = 0;
        while(timeSinceRefresh < timeBetweenRefreshes){
          if(displayRoutesEveryXMS>0){
            view.displayPrediction(address, data.getPredictions(), displayRoutesForXMs);
            delay(displayRoutesEveryXMS);
            timeSinceRefresh+=displayRoutesEveryXMS;
          }else{
            view.displayPrediction(address, data.getPredictions(), 0);
            delay(timeBetweenRefreshes);
            timeSinceRefresh += timeBetweenRefreshes;
          }
          
          
        }
        
    }
    
    
}
}



/*TODO:

*   IMPLEMENT SOME SORT OF TIMER IN THE LOOP WHICH CAN UPDATE THE TEXT
*   Finish unit tests, enabling development even when bus lines are down,
*   Instructions for inputting new stopIDS
          Direct from routemaps https://www.townofchapelhill.org/Home/ShowDocument?id=40291 NO! These are incomplete and only offer ids for specific stops called "timepoints"
          Crappy transloc map https://triangle.transloc.com/
*   OTA Updates
*   Encasement
*   10 device beta test


 XML based python solution https://github.com/rv-kip/py-talking-nextbus/blob/master/talking-nextbus.py 
*   Add custom input to setup
        a=
        stopID= (hardcoded map?)
*
*   this is literally my project in a different form and it has its own route finder in python https://github.com/adafruit/TinyXML/blob/master/examples/NextBus/NextBus.ino
*/

/* CONFIGURING PSK NET

  Get PSK here: https://help.unc.edu/help/what-is-the-unc-psk-ssid/
  Register MAC address with campus DHCP: https://nit.net.unc.edu/dhcp/

*/
/* JSON CONFIG FILE (this is onlu somewhat accurate now, it includes many more settings)
{  
   "Displays":{  
      "0":{  
         "Location":"MLK",
         "StopIDs":[  
            3450,
            2825
         ],
         "TravelTimeMin":1,
         "BadBusRoutes":[  

         ]
      },
      "1":{  
         "Location":"Longview",
         "StopIDs":[  
            4001
         ],
         "TravelTimeMin":2,
         "BadBusRoutes":[  

         ]
      }
   }
}

  // good D route for avery stop ID 3258 "dirTag":"mannunch",
  //  bad D route for avery stop ID 3257 "dirTag":"oldseast",
  // How to remove all other stops from 3, filter all routetags

  //https://triangle.transloc.com/
  //sorority stop 3258
      Total routes  : CL , SRG, TWkend, F, SRT, FG, DM, D
      Take to campus : D, TWkend
      Ignore routes : SRG,F,SRT,FG,DM
      ! dirtags : (CL)
      
  //planatarium stop 3257
       Total routes  : Vsat , U, F, SRJ, CL, DM, D
       Take to campus : U, CL
      Ignore routes : SRJ,Vsat
      ! dirtags : oldseast (D)


  //Travel time (The fastest in minutes you could ever make it to the bus stop, any busses ariving sooner than this WILL NOT BE DISPLAYED) : 2
  //stops (stopids, can be found on https://triangle.transloc.com/) : 3258,3257
  //Ignore routes (any routes which come to the stops that you will never take) : F,SRJ,SRT,SRG,Vsat,FG,DM
  //Ignore routeTags (You can use this to filter between directions of the same bus line,Ex: the D comes to both stops, but the planitatium routs has tag 'oldeast') :oldeast
  //Show route tags every (how many seconds of displaying arrival times before it shows the corrisponding bus route labels): 5
*/

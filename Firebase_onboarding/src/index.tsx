import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import {
  
  query,
  getFirestore,
  collection,
  getDocs,
  orderBy,
  startAt,
  endAt,
  QuerySnapshot,
  DocumentData,
} from "firebase/firestore/lite";
import {
  geohashForLocation,
  geohashQueryBounds,
  distanceBetween,
} from "geofire-common";

import {agencyspecificroutesterse} from "../../Global Typescript Interfaces/agencyspecificroutesterse"
import {stopspecific} from "../../Global Typescript Interfaces/stopspecific"
import {InternalRoute, InternalStop} from "../../Global Typescript Interfaces/InternalTypes"
import {ESP8266FlashSettings} from "../../Global Typescript Interfaces/ESP8266FlashSettings"


import { runTransaction } from "firebase/firestore"
import './mapfunctions.js'
import { placeStopMarkerOnMap } from "./mapfunctions.js";

import './styles/toggle-button.css';

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBLuxtHRcuOHXVZeaJmbgZU9bch15T5K0U",
  authDomain: "bus-clock-setup.firebaseapp.com",
  databaseURL: "https://bus-clock-setup-default-rtdb.firebaseio.com",
  projectId: "bus-clock-setup",
  storageBucket: "bus-clock-setup.appspot.com",
  messagingSenderId: "662772845154",
  appId: "1:662772845154:web:89e5063a59b9b665cf5f3b",
  measurementId: "G-G7H239TXCS",
};


// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
const db = getFirestore(app);

function getPosition(options?: PositionOptions): Promise<GeolocationPosition> {
  return new Promise((resolve, reject) => 
      navigator.geolocation.getCurrentPosition(resolve, reject, options)
  );
}


/**
 * [Sets the value of the agency dropdown to one of its options]
 * @param  {[string]} value [one of the agency tags already in the list]
 * @return {[void]}      
 */
function setAgency(value){
  document.getElementById("agency").setAttribute("value", value);
  document.getElementById("agency_list").setAttribute("value", value);
  let element  = document.getElementById("agency_list");
  (element as HTMLSelectElement).value = value;
}

function getAgency(){
  let element  = document.getElementById("agency_list");
  let selectedAgencyTag = (element as HTMLSelectElement).value 
  return selectedAgencyTag
}

function setStops(value : string){
  document.getElementById("stopID").setAttribute("value", value);
}

function getStops(){
  return document.getElementById("stopID").getAttribute("value");
}

function setIgnoredRoutes(value){
  document.getElementById("IgnoreRoutes").setAttribute("value", value);
}

function getIgnoredRoutes(){
  return document.getElementById("IgnoreRoutes").getAttribute('value');
}

function addIgnoredRoute(routeTag:string){
  const previousValues = getIgnoredRoutes()
  
  if(previousValues){
    let ignoredRoutes = getIgnoredRoutes().split(",").filter(e => e)
    if(ignoredRoutes.includes(routeTag) === false){
      ignoredRoutes.push(routeTag)
    }
    setIgnoredRoutes(ignoredRoutes.join(","))
  }else{
    setIgnoredRoutes(routeTag + ",")
  }
  
  
  console.log("added")
}

function removeIgnoredRoute(routeTag:string){
  let ignoredRoutes = getIgnoredRoutes().split(",").filter(e => e)
  ignoredRoutes = ignoredRoutes.filter((tag)=>tag!==routeTag)
  setIgnoredRoutes(ignoredRoutes.join(","))
}

function handleStopIDsChanged(event){
  console.log(event)
  let array = event.detail
  setStops(array.toString())
}


function moveMapLocation(){
  //TODO implement this for non-location data users. 
  //Requires moving map to webpack file and changing callback function of map load
  //May also be more secure since it doesnt expose the key? 
}

/*TODO 
  Paginate and create completed animation
  Have the agency selection step 1 complete if geolocation is successful and prompt CONFORMATION!
  If any completed divs are clicked, uncomplete them and allow them to be recompleted
*/

/**
 * Pulls the transit agency tags from firestore and creates options elements for the user to select from]
 * @return {[void]}      
 */
function populateAgencyDropdown(){
  const list = document.getElementById("agency_list");
  if(list.childNodes.length==0){
    const querySnapshot = getDocs(collection(db, "agencies"));
    querySnapshot.then((results) =>{
      results.forEach((doc)=>{
        var newoption = document.createElement("option");
        newoption.textContent = doc.id;
        list.appendChild(newoption);
      })
    });
  }

}

function userClickedTransitAgencyConfirmed(){
  let element  = document.getElementById("agency_list");
  let selectedAgencyTag = (element as HTMLSelectElement).value 
  console.log(`User confirmed transit agency as : ${selectedAgencyTag}`)
}
function userClickedSubmitConfig(){
   
  const agency = getAgency()
  const stopIDs = getStops().split(",").map((i) => Number(i))
  const ignoreRoutes = getIgnoredRoutes().split(",")

  const finalSettings : ESP8266FlashSettings = { 
    Displays: {
      0: {
        Location: "MLK",
        StopIDs: stopIDs,
        BadBusRoutes: ignoreRoutes,
        BadDirTags : null,
        TravelTimeMin: 1,
        showRouteTagInterval: 0,
      }
    }}
  console.log(`User confirmed final settings as:`)
  console.log(finalSettings)
}

function showAgencyDropdown(reason){
  console.log(reason);

  // populateAgencyDropdown()
  // const mapContainer = document.getElementById("choose_stop_div");
  // const listContainer = document.getElementById("choose_agency_div");
  // listContainer.style.display = "inherit";
  // mapContainer.style.display = "none";
}

function resolveClosestAgency() {

 return new Promise((resolve,reject) =>{
    if(!navigator.geolocation){
      reject("Browser doesn't support Geolocation")
    }
    const radiusInM = 10 * 1000;
    getPosition({
      enableHighAccuracy: false,
      maximumAge: 30000,
      timeout: 27000
    })
    .then(async (position)=>{
        const center = [position.coords.latitude, position.coords.longitude];
        const bounds = geohashQueryBounds(center, radiusInM);
        const promises = new Array<QuerySnapshot<DocumentData>>();
        for (const b of bounds) {
          const q = query(
            collection(db, "agencies"),
            orderBy("geohash"),
            startAt(b[0]),
            endAt(b[1])
          );
          promises.push(await getDocs(q));
        }
        return Promise.all(promises);
      })
    .then((snapshots) => {
        let result_count = 0
        for (let snapshot of snapshots){
          result_count += snapshot.docs.length
        }
        
        if (result_count > 1) {
          reject(`Multiple transit agencies withing ${radiusInM} Meters`)
        }else if (result_count < 1){
          reject(`No transit agencies withing ${radiusInM} Meters`)
        }else{
          resolve(snapshots[0].docs[0].id)
        }

    });
  })

}


async function populateMapStops(agency_tag){
  var Httpreq = new XMLHttpRequest(); 
    Httpreq.open(
      "GET",
      `https://retro.umoiq.com/service/publicJSONFeed?command=routeConfig&a=${agency_tag}&terse`
      ,true)
      
      Httpreq.onerror = function (e) {
        console.error(Httpreq.statusText);
      }; 
      Httpreq.onload = function (e) {
        if (Httpreq.readyState === 4) {
          if (Httpreq.status === 200) {

            
            let stops = new Array<InternalStop>()
            let results: agencyspecificroutesterse = JSON.parse(Httpreq.responseText)
            let routes = results["route"]
            routes.forEach(route => {

              route.direction.forEach(dir=>{
                console.log(`Route: ${route["tag"]} | Dir: ${dir.title}`)
              })
              
              route["stop"].forEach(stop => {
                
                const lat = parseFloat(stop['lat']);
                const lon  = parseFloat(stop['lon']);
                let stopToAdd : InternalStop = {
                  lat : lat,
                  lon : lon,
                  stopId : stop['stopId']}
                stops.push(stopToAdd)
              });
            });

            let unique_stops = stops.filter((v,i,a)=>a.findIndex(t=>(t.stopId === v.stopId))===i)
            unique_stops.forEach(e =>placeStopMarkerOnMap(e));
           
          } else {
            console.error(Httpreq.statusText);
          }
        }
      };
    Httpreq.send(null);
}

async function retreiveAllPossibleRoutes(agency_tag: string, stopID: Array<string>) : Promise<InternalRoute[]>{

  let allPossibleRoutes = []
  var Httpreq = new XMLHttpRequest(); 

  stopID.forEach(current_stopID => {
    Httpreq.open(
      "GET",
      `https://retro.umoiq.com/service/publicJSONFeed?command=predictions&a=${agency_tag}&stopId=${current_stopID}`
      ,false)
  
      Httpreq.onerror = function (e) {
        console.error(Httpreq.statusText);
      }; 

      Httpreq.onload = function (e) {
        if (Httpreq.readyState === 4) {
          if (Httpreq.status === 200) {
  
            let results: stopspecific = JSON.parse(Httpreq.responseText)
            let ensureArray = [].concat(results.predictions);
            ensureArray.forEach((prediction)=>{
              let route_name = prediction.routeTag
              /*Potential bug:  this direction thing may be an array 
               *if it services the same route doing a sort of figure 8? 
               */
              let direction_name;
              direction_name = 
              prediction.dirTitleBecauseNoPredictions ?
              prediction.dirTitleBecauseNoPredictions :
              prediction.direction.title
              
              //Dir tag is inaccessable if no Predictions. Need to filter by name in esp8266.
              //Different routes can have the same direction title, so we cant JUST filter by title
              allPossibleRoutes.push({routeTag: route_name, direction_full: direction_name})
            })
           
          }else {
            console.error(Httpreq.statusText);
          }
        }
      };
    Httpreq.send(null);
  });

  return allPossibleRoutes
}
async function addAllPossibleRoutesToDOM(agency,stops){
  let routes : InternalRoute[] = await retreiveAllPossibleRoutes(agency,stops)
  console.log(routes)
  let container = document.getElementById("route-options-container")
  routes.forEach((r)=>{
    const el = createRouteElement(r)
    el.addEventListener("click",handleRouteCheckboxChange)
    container.appendChild(el)
    addIgnoredRoute(r.routeTag)
  })
}

function createRouteElement(route: InternalRoute){
  let div = document.createElement('div');
  let generateUniqueID = function (p) {var c = 0,i;p = (typeof p==="string")?p:"";do{i=p+c++;}while(document.getElementById(i)!==null);return i;}
  let uniqueID = generateUniqueID("cb")
  div.className = "route-option"
  div.innerHTML = `
    <div class="transit-icon-container">
      <object type="image/svg+xml" data="./images/bus-symbol.svg" class="transit-icon"></object>
    </div>
    <span class="route-tag">${route.routeTag}</span>
    <span class = "route-direction"> Direction: ${route.direction_full}</span>
    <div class="transit-ignore-container">
      <input class="tgl tgl-flip" id=${uniqueID} type="checkbox" />
      <label class="tgl-btn" data-tg-off="Nope" data-tg-on="Yeah!" for=${uniqueID}></label>
    </div>
  `
  return div
}

function handleRouteCheckboxChange(e: Event){
  let divElementFromCreateRouteFunction  = e.currentTarget as HTMLElement
  let clickedBox = divElementFromCreateRouteFunction.querySelector(".tgl-flip") as HTMLInputElement
  let routeTag =  (divElementFromCreateRouteFunction.querySelector(".route-tag") as HTMLElement).innerHTML
 
  if(clickedBox.checked === false){
    addIgnoredRoute(routeTag)
  }else{
    removeIgnoredRoute(routeTag)
  }
  
}

//Button clicked
function testReceiveAgency(e) {
  resolveClosestAgency()
  .then(setAgency)
  .catch(showAgencyDropdown) // implement this
  console.log("button 1 clicked");
};
function testReceiveLocations(e) {
  console.log("button 2 clicked");
  let agencyTag = "chapel-hill" //TODO: derive agency tag from testReceiveAgency
  populateMapStops(agencyTag)
};

function testReceivePredictions(e) {
  console.log("button 3 clicked");
  let agencyTag = "chapel-hill" //TODO: derive agency tag
  let home_stopIDs = ['3450', '2825'] //TODO: derive stops tag 
  let sosority_StopIDs = ['3258'] //TODO: derive stops tag 
  addAllPossibleRoutesToDOM(agencyTag,home_stopIDs)
  //TODO: Next Step
};

document
  .getElementById("retreive-agency")
  .addEventListener("click", testReceiveAgency);
document
  .getElementById("retreive-locations")
  .addEventListener("click", testReceiveLocations);
document
  .getElementById("retreive-predictions")
  .addEventListener("click", testReceivePredictions);


document
  .getElementById("test-color-change")
  .addEventListener('click', (x)=>(document.querySelector(".transit-icon") as any).getSVGDocument().getElementById("svgInternalID").setAttribute("fill", "red"))
document.addEventListener('stops-changed', handleStopIDsChanged)
document.addEventListener("DOMContentLoaded", populateAgencyDropdown); 
document
  .getElementById("confirm_agency")
  .addEventListener("click", userClickedTransitAgencyConfirmed)
document
  .getElementById("agency_list")
  .addEventListener("change", (e)=> setAgency((e.target as HTMLTextAreaElement).value));

  document
  .getElementById("submit_config")
  .addEventListener("click", userClickedSubmitConfig)




window.initMap = initMap;

let map, infoWindow;

let MARKER_ARRAY = []

function initMap() {
  var myOptions = {
    center: { lat: -34.397, lng: 150.644 },
    zoom: 17,
    minZoom:9,
    mapTypeId: google.maps.MapTypeId.ROADMAP,

    panControl: true,
    mapTypeControl: false,
    panControlOptions: {
      position: google.maps.ControlPosition.RIGHT_CENTER
    },
    zoomControl: true,
    zoomControlOptions: {
      style: google.maps.ZoomControlStyle.LARGE,
      position: google.maps.ControlPosition.RIGHT_CENTER
    },
    scaleControl: false,
    streetViewControl: false,
    
  };
  map = new google.maps.Map(document.getElementById("mapCanvas"), myOptions);
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          };
          map.setCenter(pos);
        },
        () => {
          handleLocationError(true, infoWindow, map.getCenter());
        }
      );
    } else {
      // Browser doesn't support Geolocation
      handleLocationError(false, infoWindow, map.getCenter());
    }
}
window.initMap = initMap; //this lets initMap be a callback
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(
    browserHasGeolocation
      ? "Error: The Geolocation service failed."
      : "Error: Your browser doesn't support geolocation."
  );
  infoWindow.open(map);
}

export function placeStopMarkerOnMap(stopObject){
  const marker = new google.maps.Marker({
    position: {lat:stopObject.lat,lng:stopObject.lon},
    map,
    title: String(stopObject.stopId),
  });
  MARKER_ARRAY.push(marker);
  tieMarkerInWithIndex(marker)
}

/**
 * 
 * @param {google.maps.Marker} markerClicked 
 * @returns Array of google.maps.Marker which might overlap with the markerClicked
 */
export function requestCloseMarkersTo(markerClicked){
  const acceptable_distance_in_km = 0.1
  const lat1 = markerClicked.position.lat()
  const lon1 = markerClicked.position.lng()

  const  R = 6371  // radius of the earth in km
  let resultsArray = []
  MARKER_ARRAY.forEach(marker => {
    const lat2 = marker.position.lat()
    const lon2 = marker.position.lng()
    const x = (lon2 - lon1) * Math.cos( 0.5*(lat2+lat1) )
    const y = lat2 - lat1
    const d = R * Math.sqrt( x*x + y*y )
  
    if( d <= acceptable_distance_in_km){
      resultsArray.push(marker)
    }
  });
  
  return resultsArray

}

function tieMarkerInWithIndex(marker){
  marker.addListener("click", () => {
    let stopsArray = requestCloseMarkersTo(marker).map((marker)=>marker.title)
    const stopsChanged = new CustomEvent('stops-changed', {detail: stopsArray});
    document.dispatchEvent(stopsChanged)
  });
}
const path = require('path');

// something about proxying firebase emulators https://github.com/firebase/firebase-tools/issues/1676
module.exports = {
  mode: 'development',
  // The entry point file described above
  entry: './src/index.tsx',
  // The location of the build folder described above
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'webpackbundle.js'
  },
  // Optional and for development only. This provides the ability to
  // map the built code back to the original source format when debugging.
  devtool: "source-map",
  devServer: {
   static: './public',
   hot: true,
  },
  resolve: {
    // Add '.ts' and '.tsx' as resolvable extensions.
    extensions: ["", ".webpack.js", ".web.js", ".ts", ".tsx", ".js"],
  },
  module: {
    rules: [
      // All files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'.
      { test: /\.tsx?$/, loader: "ts-loader" },
      // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
      { test: /\.js$/, loader: "source-map-loader" },
      { test:/\.css$/,use:['style-loader','css-loader']}
    ],
  },
};
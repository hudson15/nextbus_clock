import urllib.request, json 
import time
import os
import datetime
import pygeohash as gh

class Latlng(object):
                def __init__(self, lat: float, lng: float):
                    self.lat = lat
                    self.lng = lng
class AgencyInfo(object):
                def __init__(self, tag: str, geoInfo: Latlng):
                    self.tag = tag
                    self.geoInfo = geoInfo


FILE_NAME = "update_agency_output.json"

def loadExistingAgencyCache():
    f = open(FILE_NAME,)
    return json.load(f)

def updateAndReturnAgencyCache():
    #get agencies
    with urllib.request.urlopen('https://retro.umoiq.com/service/publicJSONFeed?command=agencyList') as url:
        data = json.loads(url.read().decode())
        
        #assume for now all agencies have less than 100 routes. TODO
        #if the agency has more than 100 routes this will need to be used to loop through them all
        # for agency in data["agency"]:
        #     tag = agency["tag"]
        #     with urllib.request.urlopen(f"https://retro.umoiq.com/service/publicXMLFeed?command=routeList&a={tag}") as url:
        #         data = json.loads(url.read().decode())
        #         
    agencies = []
    for agency in data["agency"]:
        tag = agency["tag"]
        with urllib.request.urlopen(f"https://retro.umoiq.com/service/publicJSONFeed?command=routeConfig&a={tag}&terse") as url:
            data = json.loads(url.read().decode())

        
        
            allLatsInAgency = []
            allLongsInAgency=[]

            
            if "route" in data:
                if "latMax" in data["route"]:
                    allLatsInAgency.append(float(data["route"]['latMax']))
                    allLatsInAgency.append(float(data["route"]['latMin']))

                    allLongsInAgency.append(float(data["route"]["lonMax"]))
                    allLongsInAgency.append(float(data["route"]["lonMin"]))
                else:
                    for route in data["route"]:

                        allLatsInAgency.append(float(route['latMax']))
                        allLatsInAgency.append(float(route['latMin']))

                        allLongsInAgency.append(float(route["lonMax"]))
                        allLongsInAgency.append(float(route["lonMin"]))
                    
                averageLat = sum(allLatsInAgency)/len(allLatsInAgency)
                averageLon = sum(allLongsInAgency)/len(allLongsInAgency)

            
                
                geoInfo = Latlng(lat=averageLat, lng=averageLon) 
                agencyInfoInstance = AgencyInfo(tag=tag, geoInfo=geoInfo)  
                print(json.dumps(agencyInfoInstance, default=lambda o: o.__dict__, indent=4))
                agencies.append(agencyInfoInstance)
            elif "Error" in data:
                #todo catch this circumstance and handle it, could just be lasy and take one lat/lng from the first route
                #'Error': {'shouldRetry': 'false', 'content': 'Command would return more routes than the maximum: 100.\n Try specifying batches of routes from "routeList".'}}
                print(data)
                
            else:
                print("Error unhandled circumstance")
                print(data)
            

            

            time.sleep(2)

    with open(FILE_NAME, 'w', encoding='utf-8') as f:
        json.dump(agencies, f, ensure_ascii=False, default=lambda o: o.__dict__, indent=4)
    return agencies

agencies = [] ##Allow for 7 day cache
if datetime.datetime.fromtimestamp(os.path.getmtime(FILE_NAME)) > datetime.datetime.now()-datetime.timedelta(days=7):
    agencies = loadExistingAgencyCache()
else:
    agencies = updateAndReturnAgencyCache()

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

# Use the application default credentials
cred = credentials.Certificate('bus-clock-setup-firebase-adminsdk-x1k9r-f5936cdcf1.json')
firebase_admin.initialize_app(cred, {
  'projectId': "bus-clock-setup",
})

db = firestore.client()

for agency in agencies:
    
    print(agency)
    doc_ref = db.collection(u'agencies').document(agency["tag"])
    doc_ref.set({
        u'lat': agency["geoInfo"]["lat"],
        u'lng': agency["geoInfo"]["lng"],
        u'geohash' : gh.encode(agency["geoInfo"]["lat"], agency["geoInfo"]["lng"], precision=10)
    })